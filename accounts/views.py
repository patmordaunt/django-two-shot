from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm

def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)

def user_logout(request):
    logout(request)
    return redirect("login")


# def user_logout(request):
#     logout(request)
#     return redirect("home")

# def signup(request):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data['username']
#             password = form.cleaned_data['password']
#             user = authenticate(request, username=username, password=password)
#                 if user is not None:
#                     login(request, user)
#                     return redirect("home")

#                 return redirect("home")
#     else:
#         form = SignUpForm()
#         context = {
#             "form": form,
#         }
#     return render(request, "accounts/base.html", context)


# # # Create your views here.

# # # my attempt
# # def register(request):
# #     if request.method == "POST":
# #         form = UserForm(request.POST)
# #         if form.is_valid():
# #             form.save()
# #             return redirect("home")
# #     else:
# #         form = UserForm()

# # return render(request, "register.html", {"form": form})

# # # References Notes on CreateView

# # def create_new_user(request):
# #   if request.method == "POST":
# #     form = ModelForm(request.POST)
# #     if form.is_valid():
# #       form.save()
# #       # If you need to do something to the model before saving,
# #       # you can get the instance by calling
# #       # model_instance = form.save(commit=False)
# #       # Modifying the model_instance
# #       # and then calling model_instance.save()
# #       return redirect("some_url")
# #   else:
# #     form = ModelForm()

# #   context = {
# #     "form": form
# #   }

# #   return render(request, "model_names/create.html", context)
