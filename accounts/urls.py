from django.urls import path
# from django.contrib.auth import authenticate, login, logout
from .views import user_login, user_logout

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    ]

# Need to build paths for Logout and Signup
